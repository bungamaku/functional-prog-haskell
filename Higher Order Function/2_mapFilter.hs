-- [x + 1 | x <- xs]
numberOne xs = map (+1) xs

-- [x+y|x<-xs,y<-ys]
numberTwo xs ys = concat (map (\x -> map (+x) ys) xs)

-- [x+2|x<-xs,x>3]
numberThree xs = map (+2) (filter (>3) xs)

-- [x+3|(x,_)<-xys]
-- numberFour xys = map (\(x, _) -> x+3) xys

-- [x+4|(x,y)<-xys,x+y<5]
-- numberFive xys = map (\(x, y) -> x+4) (filter (<5) (map (\(x, y) -> x+y) xys))

-- [x + 5 | Just x <- mxs]
-- numberSix mxs = map (\(Just x) -> x) (filter (\x -> x /= Nothing) mxs)

-- map (+3) xs
numberSeven xs = [x + 3 | x <- xs]

-- filter (>7) xs
numberEight xs = [x | x <- xs, x > 7]

-- concat (map (\x -> map (\y -> (x,y)) ys) xs)
numberNine xs ys = [(x, y) | x <- xs, y <-ys]

-- filter (>3) (map (\(x,y) -> x+y) xys)
numberTen xys = [x + y | (x, y) <- xys, x + y > 3 ]