mystery xs = foldr (++) [] (map sing xs)
  where
    sing x = [x]

-- the function maps each element into a single-valued array that contains the element itself
-- and then concatenate each array so the result would be the same as the input array