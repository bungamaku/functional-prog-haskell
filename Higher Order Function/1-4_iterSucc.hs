-- \n -> iter n succ

-- type
-- Enum a => Int -> a -> a

-- effect
-- increase a value by one in n times, succ in the successor function