map1 xs = map (+1) (map (+1) xs)
-- return an array that increments each element by 2 from array xs

-- map f (map g xs)
-- equals to map (f . g) xs