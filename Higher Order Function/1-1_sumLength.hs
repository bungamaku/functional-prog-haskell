sumLength :: [a] -> Int
sumLength aList = sum (map (\x -> 1) aList)