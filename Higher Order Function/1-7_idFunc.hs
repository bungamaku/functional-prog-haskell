-- If id is the polymorphic identity function, defined by id x = x, explain the behavior of the expressions
-- (id . f) (f . id) (id f)
-- If f is of type Int -> Bool, at what instance of its most general type a -> a is id used in each case?

-- (id . f) x = (id :: Bool -> Bool) (f x) = f x
-- (f . id) x = f ((id :: Int -> Int) x) = f x
-- (id f) = (id :: (Int -> Bool) -> (Int -> Bool)) f = f