-- composeList receive a list of functions and return a single function

composeList :: [(a -> a)] -> (a -> a)
composeList [] = id
composeList (x:xs) = x . composeList xs 