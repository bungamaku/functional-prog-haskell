import Data.List

perm [] = [[]]
perm ls = [x : sisa | x <- ls, sisa <- perm(ls \\ [x])]