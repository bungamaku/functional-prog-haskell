data Move = Paper | Rock | Scissors deriving Show

beats :: Move -> Move
beats Paper    = Scissors
beats Rock     = Paper
beats Scissors = Rock
