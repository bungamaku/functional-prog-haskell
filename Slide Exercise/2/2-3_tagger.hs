data Tagger = Tagn Integer | Tagb Bool

number  (Tagn n) = n
boolean (Tagb b) = b

isNum (Tagn _) = True
isNum (Tagb _) = False

isBool x = not (isNum x)
