data Temp = Celsius  Float | Fahrenheit  Float | Kelvin  Float deriving Show

toKelvin (Celsius c)    = Kelvin (c + 272.0)
toKelvin (Fahrenheit f) = Kelvin ( 5/9*(f-32.0) + 272.0 )
toKelvin (Kelvin k)     = Kelvin k
