listSum, listSum', listProd, listProd' :: [Integer] -> Integer

-- In general:
-- f x = e x
-- is equal to
-- f = e
-- only if x does not appear free in e.

-- Consider:
-- f x = g (x+2) y x
-- This is not equal to:
-- f = g (x+2) y
-- because to do so might change the value of x.

listSum xs = foldr (+) 0 xs
listProd xs = foldr (*) 1 xs

listSum' = foldr (+) 0
listProd' = foldr (*) 1

-- -------------------------------

and, and', or, or' :: [Bool] -> Bool

and xs = foldr (&&) True  xs
or xs = foldr (||) False xs

and' = foldr (&&) True
or' = foldr (||) False
