import Data.Maybe

safeDivide :: Int -> Int -> Maybe Int
safeDivide x 0 = Nothing
safeDivide x y = Just (x `div` y)

-- data Maybe a = Nothing | Just a
-- Nothing :: Maybe a
-- Just :: a -> Maybe a
-- Just 3 :: Maybe Int
-- Just “x“ :: Maybe String
-- Just (3,True) :: Maybe (Int,Bool)
-- Just (Just 1) :: Maybe (Maybe Int)
