simple x y z = x * (y + z)

-- simple (simple 2 3 4) 5 6
-- => simple (2 * (3 + 4)) 5 6
-- => simple (2 * 7) 5 6
-- => simple 14 5 6
-- => 14 * (5 + 6)
-- => 14 * 11
-- => 154

-- simple (a - b) a b
-- => (a - b) * (a + b)
-- => (a * a) + (a * b) - (b * a) - (b * b) {- distributive law -}
-- => (a * a) - (b * b)
-- => a^2 - b^2