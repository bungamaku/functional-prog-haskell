pi' = 3.14159

totalArea r1 r2 r3 = (pi*(r1^2)) + (pi*(r2^2)) + (pi*(r3^2))
totalArea' r1 r2 r3 = listSum [circArea r1, circArea r2, circArea r3]

listSum [] = 0
listSum (a:as) = a + listSum as

circArea r = pi'*(r^2)